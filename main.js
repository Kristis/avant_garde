document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
  });
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });

 google.charts.load('current', {'packages':['corechart']});
 google.charts.setOnLoadCallback(drawChart);

     function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Metai', 'Pergalių skaičius', {role:'style'}],
          ['2013', 11, 'color:blue'],
          ['2014', 9, 'color:blue'],
          ['2015', 4, 'color:blue'],
          ['2016', 10, 'color:blue'],
          ['2017', 12, 'color:blue']
        ]);
     var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
        
var options = {
        title: "Pergalių skaičius stalo žaidimų turnyruose",
        hAxis: {title: 'Metai'},
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };

        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);

      }
           

google.charts.setOnLoadCallback(drawChart2);
function drawChart2 () {
        var data = google.visualization.arrayToDataTable([
          ['Metai', 'win', 'lose'],
          ['2013',  11,  9],
          ['2014',  9,  11],
          ['2015',  4,  16],
          ['2016',  10, 10],
          ['2017',  12,  8]
        ]);

        var options = {
          title: 'Pergalės ir pralaimėjimai',
          hAxis: {title: 'Metai',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }




